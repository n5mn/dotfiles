#!/bin/sh

# Pacman
packages_pacman="pacman-contrib alacritty starship vim rofi dunst lxappearance-gtk3 breeze breeze-gtk tumbler cliphist ttf-font-awesome ttf-fira-sans ttf-fira-code ttf-firacode-nerd python-pip pavucontrol xdg-desktop-portal-gtk xdg-desktop-portal-wlr networkmanager network-manager-applet networkmanager-openvpn ufw blueman bluez bluez-utils hyprland waybar jq polkit cpio"
optional_packages_pacman="firefox obs-studio discord spotify-launcher fastfetch gimp htop steam"

# AUR
packages_aur="bibata-cursor-theme kora-icon-theme swww wlogout"
optional_packages_aur="termius brave-bin qownnotes pfetch vesktop"
